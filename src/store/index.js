import Vue from 'vue';
import Vuex from 'vuex';

/* Modules index - imports all modules in molules folder */
import modules from './modules';

Vue.use(Vuex);

const store = new Vuex.Store({ modules });

export default store;
