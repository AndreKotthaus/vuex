/* eslint-disable arrow-parens */
const requireModule = require.context('.', false, /\.js$/);
const modules = {};

requireModule.keys().forEach(item => {
  if (item === './index.js') return;
  const moduleName = item.replace(/(\.\/|\.js)/g, '');
  modules[moduleName] = requireModule(item).default;
});

export default modules;
