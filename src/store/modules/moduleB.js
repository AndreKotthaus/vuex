/**
 * Talk to ModuleA :
 * -->> dispatch('moduleA/actionsName', {pay:load,foo:bar}, { root: true });
 * -->> commit('moduleA/mutationsName', {pay:load,foo:bar}, { root: true });
 */

export default {
  namespaced: true,
  state: {
    message: 'WAR NICHT SO GEMEINT :-)',
  },
  getters: {
    getMessage(state) {
      return state.message.toLowerCase();
    },
  },
  mutations: {},
  actions: {},
};
