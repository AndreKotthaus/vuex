/**
 * Talk to ModuleB :
 * -->> dispatch('moduleB/actionsName', {pay:load,foo:bar}, { root: true });
 * -->> commit('moduleB/mutationsName', {pay:load,foo:bar}, { root: true });
 */

export default {
  namespaced: true,
  state: {
    message: 'geh kacken Junge!',
  },
  getters: {
    getMessage(state) {
      return state.message.toUpperCase();
    },
  },
  mutations: {},
  actions: {},
};
